import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EcommerceListEquipmentsComponent } from './component/ecommerce-list-equipments/ecommerce-list-equipments.component';
import { EcommerceEquipmentDetailComponent } from './component/ecommerce-equipment-detail/ecommerce-equipment-detail.component';
import { CheckoutComponent } from './component/checkout/checkout.component';

const routes: Routes = [
  {
    path: '', component: EcommerceListEquipmentsComponent,
  },
  {
    path: 'details/:idProduct', component: EcommerceEquipmentDetailComponent
  },
  {
    path: 'checkout', component: CheckoutComponent
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
