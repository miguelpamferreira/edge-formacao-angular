import { Product } from './product';

export class Phone extends Product {
    public ImgUrl: string;
    public IsAvailable: boolean;
    public Color: string[];
    public Quantity: number;
}
