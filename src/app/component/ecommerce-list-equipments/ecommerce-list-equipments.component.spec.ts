import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcommerceListEquipmentsComponent } from './ecommerce-list-equipments.component';

describe('EcommerceListEquipmentsComponent', () => {
  let component: EcommerceListEquipmentsComponent;
  let fixture: ComponentFixture<EcommerceListEquipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcommerceListEquipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcommerceListEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
