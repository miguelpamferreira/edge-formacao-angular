import { Component, OnInit } from '@angular/core';
import { Phone } from '../../model/phone';
import { EcommerceService } from 'src/app/services/ecommerce.service';

@Component({
  selector: 'app-ecommerce-list-equipments',
  templateUrl: './ecommerce-list-equipments.component.html',
  styleUrls: ['./ecommerce-list-equipments.component.css']
})
export class EcommerceListEquipmentsComponent implements OnInit {

  public myPhones: Phone[];
  public quantity = 0;

  constructor(
    private ecommerceService: EcommerceService
  ) { }

  ngOnInit() {
    const phonesObservable = this.ecommerceService.getPhones();
    phonesObservable.subscribe((phones: Phone[]) => {
      this.myPhones = phones;
    });
  }

}
