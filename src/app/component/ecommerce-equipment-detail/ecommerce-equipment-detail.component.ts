import { Component, OnInit, Input } from '@angular/core';
import { Phone } from '../../model/phone';
import { ActivatedRoute } from '@angular/router';
import { EcommerceService } from 'src/app/services/ecommerce.service';


@Component({
  selector: 'app-ecommerce-equipment-detail',
  templateUrl: './ecommerce-equipment-detail.component.html',
  styleUrls: ['./ecommerce-equipment-detail.component.css']
})
export class EcommerceEquipmentDetailComponent implements OnInit {

  public myPhones: Phone[];
  public quantity = 0;
  @Input() public phone: Phone;

  constructor(
    private route: ActivatedRoute,
    private ecommerceService: EcommerceService
  ) { }

  ngOnInit() {
    this.myPhones = this.ecommerceService.getPhones();

    if (
      this.route.snapshot.paramMap.get('idProduct') !== null
      && this.route.snapshot.paramMap.get('idProduct') !== undefined
      && this.route.snapshot.paramMap.get('idProduct') !== '') {
        this.phone = this.myPhones.find(x => x.Id.toString() === this.route.snapshot.paramMap.get('idProduct'));
    }
  }

  private decrementQuantity(pho: Phone): void {
    if (this.quantity > 0) {
      pho.Quantity++;
      this.quantity--;
    }
  }

  private incrementQuantity(pho: Phone): void {
    if (pho.Quantity > 0) {
      pho.Quantity--;
      this.quantity++;
    }
  }

}
