import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcommerceEquipmentDetailComponent } from './ecommerce-equipment-detail.component';

describe('EcommerceEquipmentDetailComponent', () => {
  let component: EcommerceEquipmentDetailComponent;
  let fixture: ComponentFixture<EcommerceEquipmentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcommerceEquipmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcommerceEquipmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
