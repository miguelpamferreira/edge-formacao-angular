import { Injectable } from '@angular/core';
import { Phone } from '../model/phone';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EcommerceService {

  public myPhones: Phone[];

  constructor() {
    this.myPhones = [
      {
        Id: 1,
        Name: 'Samsung Galaxy S10',
        Price: 919.99,
        ImgUrl: '//imagesn-static.nos.pt/image5wfm3s_11464.png',
        IsAvailable: true,
        Color: ['Vermelho', 'Azul'],
        Quantity: 1
      },
      {
        Id: 2,
        Name: 'iPhone 11',
        Price: 819.99,
        ImgUrl: '//imagesn-static.nos.pt/image7mmrjo_11780.jpg',
        IsAvailable: true,
        Color: ['Vermelho', 'Preto'],
        Quantity: 1,
      },
      {
        Id: 3,
        Name: 'Sony Xperia 1',
        Price: 959.99,
        ImgUrl: '//imagesn-static.nos.pt/imagecawpyl_11534.png',
        IsAvailable: false,
        Color: ['Preto', 'Branco'],
        Quantity: 1
      }
    ];
  }

  /**
   * getPhones
   */
  public getPhones(): any {
    const phonesObservable = new Observable(observer => {
      setTimeout(() => {
        observer.next(this.myPhones);
      }, 1750);
    });
    return phonesObservable;
  }

}
