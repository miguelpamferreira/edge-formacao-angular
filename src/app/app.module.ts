import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EcommerceListEquipmentsComponent } from './component/ecommerce-list-equipments/ecommerce-list-equipments.component';
import { EcommerceEquipmentDetailComponent } from './component/ecommerce-equipment-detail/ecommerce-equipment-detail.component';
import { HeaderComponent } from './component/header/header.component';
import { CheckoutComponent } from './component/checkout/checkout.component';

@NgModule({
  declarations: [
    AppComponent,
    EcommerceListEquipmentsComponent,
    EcommerceEquipmentDetailComponent,
    HeaderComponent,
    CheckoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
